package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Assume;
import org.junit.BeforeClass;
import org.junit.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;
import junit.framework.AssertionFailedError;

public class TestFacturaMasCara {

	static GestorContabilidad gestor= new GestorContabilidad();
	static Factura unafactura;
	/*
	 * Generamos 3 facturas
	 */
	@BeforeClass
	public static void generarFacturas(){
		Cliente uncliente= new Cliente("qasad", "juan a", LocalDate.now());
		
		unafactura= new Factura(2, 1);
		Factura unafactura1= new Factura(2, 5);
		Factura unafactura2= new Factura(2, (float) 2.5);
		
		unafactura.setUncliente(uncliente);
		unafactura1.setUncliente(uncliente);
		unafactura2.setUncliente(uncliente);
		
		gestor.getvFacturas().add(unafactura);
		gestor.getvFacturas().add(unafactura1);
		gestor.getvFacturas().add(unafactura2);
	}
	
	/*
	 * Factura mas cara
	 */
	@Test
	public void facturaMasCara(){
		
	Factura esperada= gestor.facturaMascara();
	Factura unafactura1=gestor.getvFacturas().get(1);
	assertEquals(unafactura1, esperada);
	}
	/*
	 * Facturacion de un a�o
	 */
	@Test
	public void FacturaAnual(){
		
		Factura unafactura= new Factura(2, 1,"1990");
		Factura unafactura1= new Factura(2, 5,"1990");
		Factura unafactura2= new Factura(2, (float) 2.5,"2000");
		
		gestor.getvFacturas().add(unafactura);
		gestor.getvFacturas().add(unafactura1);
		gestor.getvFacturas().add(unafactura2);
		float resultado;
		resultado=gestor.calcularFacAnual("1990");
		float esperado=12;
		assertEquals(esperado, resultado, 0);
	}
	//text que cuenta las facturas tiene un cliente
	@Test
	public void contarFacturasexistentes(){
		int esperado=3;
		int resultado=gestor.cantidadFacturasCliente("qasad");
		
		assertEquals(esperado, resultado);
	}
	//text eliminamos factura existente
	@Test
	public void eliminarFacturaExistente(){
		gestor.getvFacturas().clear();
		gestor.getvFacturas().add(unafactura);
		gestor.eliminarFactura("2");
		boolean x = gestor.getvFacturas().contains(unafactura);
		assertFalse(x);
		for (int i = 0; i < gestor.getvFacturas().size(); i++) {
			if(gestor.getvFacturas().get(i).getCodigoFactura().equals("2")){
				Assume.assumeTrue(gestor.getvFacturas().get(i).getCodigoFactura(),true);
				
			}
		}
		
		gestor.eliminarFactura("2");
	}
}
